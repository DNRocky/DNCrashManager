//
//  DNCrashReportManager.m
//  DNCodeConfuseDemo
//
//  Created by hushuaike on 17/8/22.
//  Copyright © 2017年 hushuaike. All rights reserved.
//

#import "DNCrashReportManager.h"
#import "DNCrashManager.h"


typedef NS_ENUM(NSInteger, DNReportType) {
    // crash 上报
    DNReporterTypeCrash = 1,
    // 性能参数上报
    DNReporterTypeCapability = 2,
    // 日志上报
    UCSReporterTypeLog = 3,
    // 自定义日志上报
    DNReporterTypeCustom = 4
};

@implementation DNCrashReportManager

+ (void)monitorCrashEvent
{
    [[DNCrashManager sharedManager]installCrashHandler];
}

+ (void)reportCrash
{
    [[DNCrashManager sharedManager]handleCrashWithBlock:^(NSArray<NSString *> *reports) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            if (reports) {
                for (NSString *report in reports) {
                    if (report.length > 0) {
                        [DNCrashReportManager uploadCrashReportWithReport:report];
                    }
                }
            }
        });
    }];
}

+ (void)uploadCrashReportWithReport:(NSString *)report
{
    [self uploadReportWithReport:report reportType:DNReporterTypeCrash];
}

+ (void)uploadReportWithReport:(NSString *)report reportType:(DNReportType)reportType
{
//    UCSLog(@"crash report: %@", report);
//    UCSReporterModel *reportModel = [[UCSReporterModel alloc] init];
//    reportModel.reportType = @(UCSReporterTypeCrash).stringValue;
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy-mm-dd HH:mm:ss"];
//    NSDate *currentDate = [NSDate date];
//    reportModel.recordTime = [formatter stringFromDate:currentDate];
//    reportModel.reportContent = report;
//    UCSReportRequest *request = [[UCSReportRequest alloc] initWithReportModel:reportModel];
//    NSString *sid = [NSString UUID];
//    [request setApiRequstSID:sid];
//    [request startWithClass:[request modelClass] completionBlockWithSuccess:^(__kindof UCSBaseRequest * _Nonnull request) {
//        UCSLog(@"crash report success");
//    } failure:^(__kindof UCSBaseRequest * _Nonnull request) {
//        UCSLog(@"crash report fail");
//    }];
}

@end
