//
//  DNCrashManager.m
//  DNCodeConfuseDemo
//
//  Created by hushuaike on 17/8/22.
//  Copyright © 2017年 hushuaike. All rights reserved.
//

#import "DNCrashManager.h"
#import "KSCrashInstallationConsole.h"
#import "KSCrash.h"

@interface DNCrashManager ()
@property (nonatomic ,strong) KSCrashInstallation *crashInstallation;
@end
@implementation DNCrashManager


+ (instancetype)sharedManager
{
    static DNCrashManager *crashManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        crashManager = [[[self class] alloc] init];
    });
    return crashManager;
}

- (void)installCrashHandler
{
    // Create an installation
    [KSCrash sharedInstance].deleteBehaviorAfterSendAll = KSCDeleteNever;
    self.crashInstallation = [KSCrashInstallationConsole sharedInstance];
    
    // Install the crash handler. This should be done as early as possible.
    // This will record any crashes that occur, but it doesn't automatically send them.
    [self.crashInstallation install];
}


- (void)handleCrashWithBlock:(void(^)(NSArray <NSString *> *reports))block
{
    [self.crashInstallation sendAllReportsWithCompletion:^(NSArray* reports, BOOL completed, NSError* error) {
        if(completed) {
            NSArray *crashReports = [reports copy];
            if ([crashReports count] > 0) {
                if (block) {
                    block(crashReports);
                }
                [[KSCrash sharedInstance] deleteAllReports];
            }
        }
    }];
}

- (void)getCrashWithBlock:(void(^)(NSArray <NSString *> *reports))block
{
    [self.crashInstallation sendAllReportsWithCompletion:^(NSArray* reports, BOOL completed, NSError* error) {
        if(completed) {
            NSArray *crashReports = [reports copy];
            if (block) {
                block(crashReports);
            }
        }
    }];
}

@end
