//
//  DNCrashManager.h
//  DNCodeConfuseDemo
//
//  Created by hushuaike on 17/8/22.
//  Copyright © 2017年 hushuaike. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DNCrashManager : NSObject

- (instancetype)init NS_UNAVAILABLE;

+ (instancetype)sharedManager;

/**
 安装Crash监听
 */
- (void)installCrashHandler;

/**
 处理Crash
 
 @param block 返回解析成苹果Crash堆栈的字符串
 */
- (void)handleCrashWithBlock:(void(^)(NSArray <NSString *> *reports))block;


/**
 检查Crash
 
 @param block block
 */
- (void)getCrashWithBlock:(void(^)(NSArray <NSString *> *reports))block;

@end
