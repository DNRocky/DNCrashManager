//
//  DNCrashReportManager.h
//  DNCodeConfuseDemo
//
//  Created by hushuaike on 17/8/22.
//  Copyright © 2017年 hushuaike. All rights reserved.
//

#import <Foundation/Foundation.h>

#warning 引入的时候需要注意库文件缺失错误！
/**
 引入的时候如果报 “_inflate", referenced from:
 -[NSData(GZip) gunzippedWithError:] in NSData+GZip.o”之类的错误，需要引入“libz.tbd”库文
 */
@interface DNCrashReportManager : NSObject

/**
 注册crash监听
 */
+ (void)monitorCrashEvent;

/**
 上报crash
 */
+ (void)reportCrash;
@end
